import sys
import re
import time
from PyQt5.QtWidgets import QMainWindow, QApplication, QFrame, QLabel, QAction, QComboBox, QFileDialog
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QBasicTimer

MIN_SPEED = 200
MAX_SPEED = 1000

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.menuBar = self.menuBar()
        self.fileMenu = self.menuBar.addMenu('&File')
        self.toolBar = self.addToolBar('Main')

        self.screen = Screen(self)

        # declare navigation items
        self.openAction = QAction('Open', self)
        self.openAction.setShortcut('Ctrl+O')
        self.openAction.triggered.connect(self.onOpenClick)

        self.exitAction = QAction('Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.triggered.connect(self.close)

        self.previousAction = QAction(QIcon('icons/previous.svg'), 'Previous', self)
        self.previousAction.setShortcut('Left')
        self.previousAction.triggered.connect(self.onPreviousClick)

        self.togglePlayAction = QAction(QIcon('icons/play.svg'), 'Play', self)
        self.togglePlayAction.setShortcut('Space')
        self.togglePlayAction.triggered.connect(self.onTogglePlayClick)

        self.nextAction = QAction(QIcon('icons/next.svg'), 'Next', self)
        self.nextAction.setShortcut('Right')
        self.nextAction.triggered.connect(self.onNextClick)

        self.speed = QComboBox()
        self.speedList = list(reversed([str(speed) + ' wpm' for speed in range(MIN_SPEED, MAX_SPEED + 1, 50)]))
        self.speed.insertItems(1, self.speedList)
        self.speed.setCurrentIndex(len(self.speedList) - 1)
        self.speed.currentIndexChanged.connect(self.onSpeedChanged)

        # add navigation items to menu
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.exitAction)

        # add navigation items to toolbar
        self.toolBar.addAction(self.previousAction)
        self.toolBar.addAction(self.togglePlayAction)
        self.toolBar.addAction(self.nextAction)
        self.toolBar.addWidget(self.speed)

        self.setWindowTitle('RSVP')
        self.setGeometry(0, 0, 530, 170)
        self.setCentralWidget(self.screen)
        self.openFile('in.txt')
        self.show()

    def onTogglePlayClick(self):
        if self.screen.isPlaying:
            self.screen.pause()
            self.togglePlayAction.setIcon(QIcon('icons/play.svg'))
        else:
            self.screen.play()
            self.togglePlayAction.setIcon(QIcon('icons/pause.svg'))

    def onNextClick(self):
        if not(self.screen.isPlaying):
            self.screen.next()

    def onPreviousClick(self):
        if not(self.screen.isPlaying):
            self.screen.previous()

    def onSpeedChanged(self, speedIndex):
        speed = int(re.search(r'\d+', self.speedList[speedIndex]).group())
        self.screen.setSpeed(speed)

    def onOpenClick(self):
        filePath = QFileDialog.getOpenFileName(self, 'Open file')[0]
        self.openFile(filePath)

    def openFile(self, filePath):
        with open(filePath, 'r') as file:
            string = file.read()

        wordsList = re.sub('\s+', ' ',  string).split()
        self.screen.setWords(wordsList)

    def showStatusMessage(self, message):
        self.statusBar().showMessage(message)

class Screen(QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.mainWindow = parent
        self.initScreen()

    def initScreen(self):
        self.isPlaying = False
        self.setSpeed(MIN_SPEED)

        self.font = QFont('Arial')
        self.font.setPixelSize(36)

        self.text = QLabel(self)
        self.text.setFont(self.font)
        self.text.setMargin(15)
        self.text.setFixedWidth(500)

    def setWords(self, wordsList):
        self.words = wordsList
        self.currentWordIndex = 0
        self.isPlaying = False

        self.timer = QBasicTimer()
        self.text.setText(self.words[self.currentWordIndex])

        self.showProgress()

    def play(self):
        self.timer.start(self.speed, self)
        self.isPlaying = True

    def pause(self):
        self.timer.stop()
        self.isPlaying = False

    def previous(self):
        if (self.currentWordIndex - 1) > 0:
            self.currentWordIndex -= 1
        else:
            self.currentWordIndex = 0

        self.text.setText(self.words[self.currentWordIndex])

        self.showProgress()

    def next(self):
        if self.isPlaying:
            self.testPunctuation()

        if (self.currentWordIndex + 1) < len(self.words):
            self.currentWordIndex += 1

        self.text.setText(self.words[self.currentWordIndex])

        self.showProgress()


    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            self.next()
        else:
            super(Screen, self).timerEvent(event)

    def setSpeed(self, speed):
        self.speed = 60000 / speed

        if self.isPlaying:
            self.pause()
            self.play()

    def showProgress(self):
        progress = 'Progress:' + '{:10.2f}'.format((self.currentWordIndex + 1) * 100 / len(self.words)) + ' %'
        self.mainWindow.showStatusMessage(progress)

    def testPunctuation(self):
        word = self.words[self.currentWordIndex]

        if re.compile(r'[?.!]').findall(word):
            delayDivider = 1000
        elif re.compile(r'[,;:\-]').findall(word):
            delayDivider = 2000
        else:
            delayDivider = 0

        if delayDivider:
            self.pause()
            time.sleep(self.speed / delayDivider)
            self.play()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())
